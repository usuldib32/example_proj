package sdacademy;

import loader.UserLoader;
import model.User;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StreamMain {
    public static void main(String[] args) throws IOException {
//        File file = new File("d:/PROGRAMY_NA_GIT/StreamExamples/src/main/resources/users.txt ");
        File file = new File("src/main/resources/users.txt ");

       List<User> users=UserLoader.getUserFromFile(file);
        System.out.println(users);
        List<User> womenList = UserLoader.getAllWomans(users);
        System.out.println(womenList);

//        List<User> users = new ArrayList<User>();
//        LineIterator fileContents = FileUtils.lineIterator(file, "UTF-8");
//        while(fileContents.hasNext()){
////Wczytanie linijki z pliku i utworzenie tablicy
////Każdy element tablicy to kolejne słowo z linijki
//            String[] line = fileContents.nextLine().split(" ");
////Utworzenie obiektu klasy User i dodanie go do listy
//            users.add(new User(line[0], line[1], Integer.parseInt(line[2])));
//        }
//        for (User user : users) {
//            System.out.println(user);
//        }
//
//
////        LineIterator fileContents = FileUtils.lineIterator(file, "UTF-8");
//        while (fileContents.hasNext()) {
//            System.out.println(fileContents.nextLine());

//            File file = new File("d:/PROGRAMY_NA_GIT/StreamExamples/src/main/resources/simpleExample.txt ");
////Otwiera strumień dostępu do pliku
//                    FileInputStream fis = new FileInputStream(file);
////Wczytanie pierwszego bajtu
//            int bajt = fis.read();
////metoda read zwraca wartość -1 po ostatnim wczytanym bajcie z pliku
//            while(bajt != -1){
////bajt jest rzutowany na znak i wypisywany w konsoli
//                System.out.print((char)bajt);
////Wczytywany jest kolejny bajt
//                bajt = fis.read();
//            }
        }
    }

