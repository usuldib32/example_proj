package loader;

import model.User;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserLoader {

    public static List<User> getUserFromFile(File file) {
        List<User> users = new ArrayList<>();
        try {   //ten try łapie wyjątek gdypy pliku nie było
            LineIterator lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String[] userRaw = lineIterator.nextLine().split(" "); //wrzuca do tablicy stringi z rozczytanej linii, separator
                User user = new User(userRaw[0],userRaw[1],Integer.valueOf(userRaw[2]));
                users.add(user);
            }

        } catch (IOException e) {
            return users;
        }
        return users;
    }

    public static List<User> getAllWomans(List<User> users){
        String name;
        int age;
        List<User> womanList = new ArrayList<>();
        for(User user : users) {
            name = user.getName();
            if (name.charAt((name.length()-1)) == 'a' && user.getAge() > 18) {
              womanList.add(user);
            }
        }
        return womanList;
    }


}
