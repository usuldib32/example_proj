package BookReadExmp;

public class Book {
    private int id;
    private String cat;
    private String name;
    private float price;
    private boolean instock;
    private String author;
    private String series;
    private int sequence;
    private String genre;

    public Book(String name, float price, boolean instock) {
        this.name = name;
        this.price = price;
        this.instock = instock;
    }

//    public boolean setAviability(String aviability) {
//        boolean isTrue;
//        boolean isFalse;
//
//
//    }


    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", instock=" + instock +
                '}' + "\n";
    }
}
