package BookReadExmp;

import model.User;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BookOptions {

    public static List<Book> getBookInfo(File file) {
        List<Book> books = new ArrayList<>();
        try {   //ten try łapie wyjątek gdypy pliku nie było
            LineIterator lineIterator = FileUtils.lineIterator(file);
            if (lineIterator.hasNext()) lineIterator.nextLine();
            while (lineIterator.hasNext()) {
                String[] bookRaw = lineIterator.nextLine().split(","); //wrzuca do tablicy stringi z rozczytanej linii, separator
//                Book book = new User(Integer.valueOf(bookRaw[0]), bookRaw[1], Float.parseFloat(bookRaw[2]), Boolean.parseBoolean(bookRaw[3]), bookRaw[4], bookRaw[5], bookRaw[6], bookRaw[7]));
                Book book = new Book(bookRaw[2], Float.parseFloat(bookRaw[3]), Boolean.parseBoolean(bookRaw[4]));
                books.add(book);
            }

        } catch (IOException e) {
            return books;
        }
        return books;
    }

}
